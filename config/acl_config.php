<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/******************************************************************************************************************
 * 
 * Copy ini di folder application/config jika ingin menambah/mengubah config. 
 * Misalkan label View ingin diganti menjadi Read, 
 * atau misalkan ingin menambah ACL baru, tambahkan di bagian bawah, dsb.
 * 
 * PERHATIAN!!
 * Jangan edit urutan dan code jika sudah ada data. Karena akan memberikan output yang salah!
 * Jika ingin menambah ACL tambahkan di bagian bawah, jangan disisipkan.
 * 
 *****************************************************************************************************************/

$config['acl_config'] = array(
    array('code' => 'view',   'label' => 'View'  ),  // 1 = 2^0
    array('code' => 'add',    'label' => 'Add'   ),  // 2 = 2^1
    array('code' => 'edit',   'label' => 'Edit'  ),  // 4 = 2^2
    array('code' => 'delete', 'label' => 'Delete'),  // 8 = 2^3
    array('code' => 'approve','label' => 'Approve'), // 16 = 2^4
    // tambah ACL baru di sini ... 2^(index)
);

/*
// Diset jika akan menggunakan method read_from_url
$config['acl_tables'] = array(
	'acls' => array(
		'table' => 'acls', // nama table ACL
		'url' => 'url', // nama field untuk url di table ACL
		'id' => 'id' // nama field untuk id di table ACL
		),
	'acls_values' => array(
		'table' => 'groups_acls', // nama table yang berisi value acl untuk GROUPS
		'acls_id' => 'acls_id', // nama field yg merujuk ke id table ACL
		'groups_id' => 'groups_id', // nama field yang merujuk ke id table GROUPS
		'value' => 'value' // nama field untuk nilai ACL
		),
);
*/
