<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acl {
	private $ci;
	private $acl_config;
	private $config;
	
	public function __construct()
	{
		$this->ci =& get_instance();

		$this->ci->config->load('acl_config', FALSE, TRUE);
		$this->acl_config = $this->ci->config->item('acl_config');
		$this->acl_tables = $this->ci->config->item('acl_tables');
		
		// Default
		if (empty($this->acl_config))
		{
			$this->acl_config = array(
				array('code' => 'view',   'label' => 'View'  ),  // 1 = 2^0
				array('code' => 'add',    'label' => 'Add'   ),  // 2 = 2^1
				array('code' => 'edit',   'label' => 'Edit'  ),  // 4 = 2^2
				array('code' => 'delete', 'label' => 'Delete'),  // 8 = 2^3
				array('code' => 'approve','label' => 'Approve'), // 16 = 2^4
				// tambah ACL baru di sini ... 2^(index)
			);
		}
		
		$this->config = array(
			'allow_all' => FALSE,
			'groups_id' => NULL
		);
	}
	
	public function get_acl_config()
	{
		$acl_config = $this->acl_config;

		foreach ($acl_config as $exponent => $row)
		{
			$acl_config[$exponent]['value'] = pow(2,$exponent);
		}
		
		return $acl_config;
	}
	
	public function read($acl_value, &$output_details=null)
	{
		$output = array();
		$total = count($this->acl_config);
		
		if (TRUE === $this->config['allow_all'])
		{
			$acl_value = (pow(2,$total)) - 1;
		}
		
		if ($total > 0)
		{
			$max = (pow(2,$total)) - 1;
			$acl_value = ($acl_value > $max) ? $max : $acl_value;
			
			for ($i = ($total - 1); $i >= 0; $i--)
			{
				if (pow(2,$i) <= $acl_value)
				{
					$code = $this->acl_config[$i]['code'];
					
					$output_details[$code] = array(
						'value' => pow(2,$i),
						'label' => $this->acl_config[$i]['label'],
					);
					
					$output[] = $code;
					$acl_value -= pow(2,$i);
				}
			}
		}
		
		return $output;
	}
	
	public function read_from_url($url=null, $groups_id = null, &$output_details=null)
	{
		if (FALSE === isset($this->acl_tables['acls'], $this->acl_tables['acls_values']))
		{
			show_error('acl_tables belum diset!');
		}
		
		if (FALSE === isset($this->ci->db))
		{
			show_error('database harus diload terlebih dahulu!');
		}
		
		if (isset($this->config['groups_id']) && $this->config['groups_id'] !== null && $groups_id === null)
		{
			$groups_id = $this->config['groups_id'];
		}
		
		if ($groups_id === null && FALSE === $this->config['allow_all'])
		{
			show_error('groups_id untuk ACL harus diisi');
		}
		
		if (FALSE === function_exists('uri_string'))
		{
			$this->ci->load->helper('url');
		}
		
		$acl_value = 0;
		
		if (FALSE === $this->config['allow_all'])
		{
			if (NULL === $url)
			{
				$part = explode('/', uri_string());

				if (count($part) == 2)
				{
					$url = preg_replace("/\/index/", '', uri_string());
				}
				else
				{
					$url = uri_string();
				}
			}
			
			$acls = $this->acl_tables['acls']['table'];
			$acls_id = $this->acl_tables['acls']['id'];
			$acls_values = $this->acl_tables['acls_values']['table'];
			$acls_values_acls_id = $this->acl_tables['acls_values']['acls_id'];
			$acls_values_users_id = $this->acl_tables['acls_values']['groups_id'];
			$acls_values_value = $this->acl_tables['acls_values']['value'];
			
			$this->ci->db->select($acls_values.'.'.$acls_values_value);
			$this->ci->db->where($acls.'.'.$this->acl_tables['acls']['url'], $url);
			$this->ci->db->where($acls_values.'.'.$acls_values_users_id, $groups_id);
			$this->ci->db->join($acls_values, "{$acls_values}.{$acls_values_acls_id} = {$acls}.{$acls_id}");
			$get = $this->ci->db->get($acls);
			
			if ($row = $get->first_row('array'))
			{
				$acl_value = $row[$acls_values_value];
			}
		}
		
		return $this->read($acl_value, $output_details);
	}
	
	public function write_from_values($acl_array)
	{
		return array_sum(array_unique($acl_array));
	}
	
	public function set_config($key, $value)
	{
		$this->config[$key] = $value;
	}
	
	public function write_from_codes($acl_array_code)
	{
		$acl_array = array();
		
		foreach (array_unique($acl_array_code) as $code)
		{
			foreach ($this->acl_config as $exponent => $row)
			{
				if ($row['code'] == $code)
				{
					$acl_array[] = pow(2,$exponent);
				}
			}
		}
		
		return array_sum($acl_array);
	}
}
